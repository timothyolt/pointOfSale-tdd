# Inbox

## Test List

### 3 items, all products found
### 3 items, some products found
### 3 items, no products found
### 0 items
### 1 item, $0 total
### 1 item found
### 1 item not found
### 3 items, one empty

## Feature ideas
* Manual Override
* Reminder to add item to catalog

## Performance
* Scan a ton of items 

```mermaid
graph
    PointOfSale --> Catalog
    PointOfSale --> Display
    Display --> Output
    PropertyOutput -. implements .-> Output
```
