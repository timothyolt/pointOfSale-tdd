package tech.materialize.pos.test

import tech.materialize.pos.Barcode
import tech.materialize.pos.Catalog
import tech.materialize.pos.PointOfSale
import tech.materialize.pos.Display
import tech.materialize.pos.Price
import tech.materialize.pos.barcode
import kotlin.test.Test
import kotlin.test.assertEquals

class EmptyBarcodeTest {
    @Test
    fun `empty barcode when empty barcode is part of the catalog`() {
        val display = Display()
        val pointOfSale = PointOfSale(
            display, Catalog(
                mapOf(
                    barcode("") to Price("$1.00")
                )
            )
        )

        pointOfSale.onBarcode(barcode(""))
        assertEquals("Error: Empty Barcode", display.text)
    }

    @Test
    fun `empty barcode`() {
        val display = Display()
        val pointOfSale = PointOfSale(display, Catalog(emptyMap()))

        pointOfSale.onBarcode(barcode(""))

        assertEquals("Error: Empty Barcode", display.text)
    }
}
