package tech.materialize.pos.test

import org.junit.jupiter.api.BeforeEach
import tech.materialize.pos.Barcode
import tech.materialize.pos.Catalog
import tech.materialize.pos.PointOfSale
import tech.materialize.pos.Display
import tech.materialize.pos.Price
import tech.materialize.pos.barcode
import kotlin.test.*

class PurchaseAnItemTest {

    private lateinit var display: Display
    private lateinit var pointOfSale: PointOfSale

    @BeforeEach
    fun setup() {
        display = Display()
        pointOfSale = PointOfSale(
            display, Catalog(
                mapOf(
                    barcode("12345") to Price("$4.20"),
                    barcode("67890") to Price("$7.89")
                )
            )
        )
    }

    @Test
    fun `product found`() {
        pointOfSale.onBarcode(barcode("12345"))
        assertEquals("$4.20", display.text)
    }

    @Test
    fun `another product found`() {
        pointOfSale.onBarcode(barcode("67890"))
        assertEquals("$7.89", display.text)
    }

    @Test
    fun `product not found`() {
        pointOfSale.onBarcode(barcode("99999"))
        assertEquals("Product not found: 99999", display.text)
    }

}
