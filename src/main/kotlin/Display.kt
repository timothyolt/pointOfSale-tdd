package tech.materialize.pos

class Display {
    var text: String? = null
        private set

    fun display(content: Content) {
        text = when (content) {
            Content.EmptyBarcode -> "Error: Empty Barcode"
            is Content.ProductFound -> content.price.text
            is Content.ProductNotFound -> "Product not found: ${content.barcode.text}"
        }
    }

    sealed interface Content {
        object EmptyBarcode : Content
        data class ProductNotFound(val barcode: Barcode) : Content
        data class ProductFound(val price: Price) : Content
    }
}
