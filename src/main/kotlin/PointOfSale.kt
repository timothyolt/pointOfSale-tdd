package tech.materialize.pos

import tech.materialize.pos.Display.Content.ProductFound
import tech.materialize.pos.Display.Content.ProductNotFound

class PointOfSale(
    private val display: Display,
    private val catalog: Catalog
) {

    fun onBarcode(barcode: AbstractBarcode) {
        display.display(
            when (barcode) {
                is EmptyBarcode -> Display.Content.EmptyBarcode
                is Barcode -> displayProduct(barcode)
            }
        )
    }

    private fun displayProduct(barcode: Barcode): Display.Content {
        val price = catalog.findPrice(barcode)
        return if (price == null) {
            ProductNotFound(barcode)
        } else {
            ProductFound(price)
        }
    }

}
