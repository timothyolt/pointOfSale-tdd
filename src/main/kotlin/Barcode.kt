package tech.materialize.pos

sealed interface AbstractBarcode

data class Barcode(val text: String) : AbstractBarcode

object EmptyBarcode : AbstractBarcode

fun barcode(text: String) = if (text == "") EmptyBarcode else Barcode(text)
