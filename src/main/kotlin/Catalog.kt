package tech.materialize.pos

class Catalog(
    private val pricesByBarcode: Map<AbstractBarcode, Price>
) {

    fun findPrice(barcode: Barcode) = pricesByBarcode[barcode]
}
